/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  HSController.m
//  headstak
//
//  Created by Veghead on 7/24/11.


#import "HSController.h"
#import "StakWindowController.h"
#import "StakDB.h"
#import "StakItem.h"
#import "StakView.h"
#import "StakItemView.h"

@implementation HSController
@synthesize swc = _swc,
            db = _db,
            stakHolder = _stakHolder;

- (id)init
{
    self = [super init];
    if (self) {
        _swc = [[StakWindowController alloc] init];
        [_swc setController:self];
        _db = [StakDB stackDB];
        [_db setTraceExecution:YES];
        [_db setLogsErrors:YES];
        [_db open];
        _stakHolder = [[NSMutableArray alloc] init];        
        [_db loadStakIntoHolder:_stakHolder]; 
    }
    return self;
}


- (void)flagsChanged:(NSEvent *)theEvent {
    NSLog(@"flags changed");
}


- (StakItem *)pushNewItemOfType:(NSUInteger)itemType {
    StakItem *item = [[StakItem alloc] initNewItemOfType:itemType InDb:[self db]];
    [[self stakHolder] insertObject:item atIndex:0];
    [[[self swc] stakView] addSubview:[item viewInStakView:[[self swc] stakView]]];
    [self layoutItemViews];
    return item;
}

- (StakItem *)popItem {
    StakItem *item = nil;
    if ([[self stakHolder] count]) {
        item = [[self stakHolder] objectAtIndex:0];
        [item setPopTime:[NSDate date]];
        [item save];
        [[self stakHolder] removeObjectAtIndex:0];
        [[item viewInStakView:[[self swc] stakView]] removeFromSuperview];
        [self layoutItemViews];
    }
    return item;
}


- (void)setStak:(NSMutableArray *)stak {
    [self setStakHolder:stak];
}

- (void)layoutItemViews {
    NSRect r = [[[self swc] stakView] frame];
    NSInteger yoffset = 0;
    NSMakeRect(r.origin.x, [[self stakHolder] count] * 40, r.size.width, r.size.height);
    for (StakItem *item in [self stakHolder]) {
        NSRect theFrame = [[item view] frame];
        theFrame.origin.y = yoffset;
        [[item view] setFrame:theFrame];
        yoffset += theFrame.size.height + 5;
    }
    [[[self swc]stakView] setNeedsDisplay:YES];
    return;
}

@end
