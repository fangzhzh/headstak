# headstak - a stak for your head

It's widely understood that [interruptions are the enemy of working effectively](http://bigthink.com/ideas/18522 "interruptions are the enemy of working effectively"), especially in my accidental career as software developer. A while ago I tried to come up with a solution to this problem which *didn't* involve working in a well designed office environment; the world is too broken to cope with such radicalism at this point. No, we must plod on in our horrendous open-plan, cheap as chips, everyone in one big roomi, mess.

So in an attempt to make it better for myself I thought of a simple program that would let you keep track of your own work and remind you where you were *before* the current interruption. So, being a geek, I came up with the idea of a stack for your brain: before you start a task, or when you are interrupted while working on a task, you *push* the current job onto a stack. When the interruption finishes, or when the task is complete, you *pop* it from the stack...leaving you facing the job you were working on beforehand at the top of the stack.

It took me a while to get anything concrete down, but there is now a prototype! ATM it's only for Mac users (sorry) but you can download it here.

Run it and it should appear on your status bar as a little stack of stuff on a head. Whenever you are about to start or finish a task you just hit the global hotkey:

    Control-Command-0

(that's a zero, not an "O").

Then you hit a down arrow to push a task, at which point you may optionally describe it. Press escape to get rid of the window.

If the phone rings, or someone in your office comes over to talk to you, push a new task.

When the most recent interruption/task is finished, hit the hot key again (Control-Command-0) and hit the up arrow to pop the most recent task.

That's it!

You can push different types of activity onto the stack - at the moment you use the down arrow for a normal task, the left arrow for a distraction and the right arrow for a sidetrack. These are, obviously, arbitrary and ideally they'll be editable. Either way, it all gets logged in a little database.

Ultimately headstak will contain tools for analysing how much time you spend on each type of task in some sort of nice graph or something.

I've been using it in earnest recently and have found it as useful as I'd initially imagined! That sort of thing cheers me up.

Please let me know what you think even if it's negative.

martin A-to-the-T dreadtech.com
